__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import clone
from sklearn.svm import SVC
from sklearn import metrics


testdata_filename = 'data/NSL/testdata'
traindata_filename = 'data/NSL/traindata'

trainData, trainTargets = KDDTools.loadDataset(traindata_filename)
testData, testTargets = KDDTools.loadDataset(testdata_filename)
print(metrics.confusion_matrix(trainTargets, trainTargets))

clf_boost = ClfTools.loadPersistent('persistent/NSL/Boosting')
clf_dTree =  ClfTools.loadPersistent('persistent/NSL/DTree')
clf_nn = ClfTools.loadPersistent('persistent/NSL/NCent')
clf_svm =  ClfTools.loadPersistent('persistent/NSL/SVM')

#model = svm.libsvm.fit(np.array(trainData).astype(float),np.array(trainTargets).astype(float),kernel='rbf',C=np.float64(32768),gamma=np.float64(8),epsilon=np.float(0.1))
if clf_boost == 0:
    print('No data for boosting found, training classifier...')
    clf_boost = AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70)
    clf_boost.fit(trainData, trainTargets)
    ClfTools.createPersistent(clf_boost, 'persistent/NSL/Boosting')
if clf_dTree == 0:
    print('No data for decision tree found, training classifier...')
    clf_dTree = DecisionTreeClassifier(max_depth=None,class_weight='auto',min_samples_leaf=20)
    clf_dTree.fit(trainData, trainTargets)
    ClfTools.createPersistent(clf_dTree,'persistent/NSL/DTree')
if clf_nn == 0:
    print('No data for nearest centroid found, training classifier...')
    clf_nn = KNeighborsClassifier(n_neighbors=1,weights='distance',algorithm='kd_tree',leaf_size=50)
    clf_nn.fit(trainData, trainTargets)
    ClfTools.createPersistent(clf_nn,'persistent/NSL/NCent')
if clf_svm == 0:
    print('No data for SVM found, training classifier...')
    clf_svm = SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, cache_size=500, verbose=True)
    clf_svm.fit(trainData, trainTargets)
    ClfTools.createPersistent(clf_svm,'persistent/NSL/SVM')

boost_out = clf_boost.predict(testData)
dTree_out = clf_dTree.predict(testData)
nn_out = clf_nn.predict(testData)
svm_out = clf_svm.predict(testData)

#print(np.shape(boost_out),np.shape(dTree_out),np.shape(nn_out),np.shape(svm_out),np.shape(testTargets))

ClfTools.printStats(testTargets,boost_out,'Boosting')
ClfTools.printStats(testTargets,dTree_out,'Decision Tree')
ClfTools.printStats(testTargets,nn_out,'Nearest Centroid')
ClfTools.printStats(testTargets,svm_out,'SVM')

#generate voted output
votes = []

#Go through every vote:
for i in range(0,len(testTargets)):
    if (nn_out[i] == 1):
        votes.append(1)
    elif (((boost_out[i] == 0) and (dTree_out[i] == 0)) or ((boost_out[i] == 0) and (svm_out[i] == 0))):
        votes.append(0)
    elif ((boost_out[i] == 1)) and (svm_out[i] == 1) or ((dTree_out[i] == 1) and (svm_out[i] == 1)):
        votes.append(1)
    else:
        votes.append(nn_out[i])



ClfTools.printStats(testTargets,votes,'Vote-Classifier')
