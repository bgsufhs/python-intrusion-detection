__author__ = 'esKay'
import csv
import pickle
import numpy
import sys
from sklearn import metrics


# Opens an already mapped KDD-File in csv format.
#Specify the name of data and targets-file
#Returns data and targets as numpy array
def openKDD(filename, targetname):
    kdd_data = numpy.genfromtxt(filename, delimiter=',')
    target_data = numpy.genfromtxt(targetname, delimiter=',')
    return kdd_data, target_data


#Opens and Maps a NSL KDD-File
#Creates two csv-files after parsing (data & targets), if save is set to true
#Mapped numpy array of targets and data is returned
def genFromNSL(filename, save=False, attack_types=False):
    kdd_data = numpy.genfromtxt(filename, delimiter=',', dtype=None)
    kdd_data, targets = KDD_NSLmapper(kdd_data,attack_types)

    #Write the file for further usage
    if save == True:
        with open(filename + '.csv', 'w', newline='') as fp:
            a = csv.writer(fp, delimiter=',')
            a.writerows(kdd_data)
        numpy.savetxt(filename + '.targets.csv', targets, delimiter=",")

    return kdd_data, targets

#Mapper function to transform string to numeric, can only be used with KDD-NSL
def KDD_NSLmapper(nsl_Data, attack_types = False):
    #There attributes are strings and need to be converted in order to be used in a classifier
    #If Attack_types is true, the KDD File including the attack types is used
    loadValue = 0
    row_count = len(nsl_Data)
    protocol_type = []
    service_type = []
    flag_type = []
    targets = []

    for i in range(row_count):  #iterate throug every row and pick an element

        #Create array of targets based on used class
        if attack_types == True:
            type = nsl_Data[i][41].decode('UTF-8')
            if (type == 'warezclient') or (type == 'warezmaster') or (type == 'guess_passwd') or (type == 'multihop') \
                    or (type == 'ftp_write') or (type == 'imap') or (type == 'phf') or (type == 'spy') or (type == 'apache2') or (type == 'worm'):
                targets.append(1) #r2l attacks
            elif (type == 'back') or (type == 'land') or (type == 'neptune') or (type == 'pod') \
                    or (type == 'smurf') or (type == 'teardrop') or (type == 'mailbomb') or (type == 'mscan') or (type == 'httptunnel'):
                targets.append(2) #dos attacks
            elif (type == 'ipsweep') or (type == 'nmap') or (type == 'portsweep') or (type == 'satan') or (type == 'snmpgetattack')or (type == 'named')or (type == 'saint') or (type == 'snmpguess'):
                targets.append(3) #probe attacks
            elif (type == 'loadmodule') or (type == 'buffer_overflow') or (type == 'perl') or (type == 'rootkit') or (type == 'xterm') or (type == 'xsnoop') or (type == 'sqlattack') or (type == 'xlock') or (type == 'sendmail') or (type == 'udpstorm') or (type == 'processtable') or (type == 'ps') or (type == 'worm'):
                targets.append(4) #U2R attacks
            elif (type == 'normal'):
                targets.append(0) #normal
            else:
                print('UNIDENTIFIED!:' + type + '  LNumber:  ' + str(i))
                targets.append(0)

        #If attack_types is true, then the attacks are mapped to their type
        if attack_types == False:
            if nsl_Data[i][41].decode('UTF-8') == 'normal':
                targets.append(0)
            else:
                targets.append(1)



        #set  protocol_type {tcp,udp, icmp} /Nr 1/
        protocol = nsl_Data[i][1].decode('UTF-8')
        if protocol == 'tcp':
            protocol_type.append(1)
        elif protocol == 'udp':
            protocol_type.append(2)
        else:
            protocol_type.append(3)

        #set service aol, auth, bgp, courier, csnet_ns, ctf, daytime, discard, domain, domain_u, echo, eco_i, ecr_i, efs, exec, finger, ftp, ftp_data, gopher, harvest
        service = nsl_Data[i][2].decode('UTF-8')
        if service == 'aol':
            service_type.append(1)
        elif service == 'auth':
            service_type.append(2)
        elif service == 'bgp':
            service_type.append(3)
        elif service == 'courier':
            service_type.append(4)
        elif service == 'csnet_ns':
            service_type.append(5)
        elif service == 'ctf':
            service_type.append(6)
        elif service == 'daytime':
            service_type.append(7)
        elif service == 'discard':
            service_type.append(8)
        elif service == 'domain':
            service_type.append(9)
        elif service == 'domain_u':
            service_type.append(10)
        elif service == 'echo':
            service_type.append(11)
        elif service == 'eco_i':
            service_type.append(12)
        elif service == 'ecr_i':
            service_type.append(13)
        elif service == 'efs':
            service_type.append(14)
        elif service == 'exec':
            service_type.append(15)
        elif service == 'finger':
            service_type.append(16)
        elif service == 'ftp':
            service_type.append(17)
        elif service == 'ftp_data':
            service_type.append(18)
        elif service == 'gopher':
            service_type.append(19)
        elif service == 'harvest':
            service_type.append(20)
        elif service == 'hostnames':
            service_type.append(21)
        elif service == 'http':
            service_type.append(22)
        elif service == 'http_2784':
            service_type.append(23)
        elif service == 'http_443':
            service_type.append(24)
        elif service == 'http_8001':
            service_type.append(25)
        elif service == 'imap4':
            service_type.append(26)
        elif service == 'IRC':
            service_type.append(27)
        elif service == 'iso_tsap':
            service_type.append(28)
        elif service == 'klogin':
            service_type.append(29)
        elif service == 'kshell':
            service_type.append(30)
        elif service == 'ldap':
            service_type.append(31)
        elif service == 'link':
            service_type.append(32)
        elif service == 'login':
            service_type.append(33)
        elif service == 'mtp':
            service_type.append(34)
        elif service == 'name':
            service_type.append(35)
        elif service == 'netbios_dgm':
            service_type.append(36)
        elif service == 'netbios_ns':
            service_type.append(37)
        elif service == 'netbios_ssn':
            service_type.append(38)
        elif service == 'netstat':
            service_type.append(39)
        elif service == 'nnsp':
            service_type.append(40)
        elif service == 'nntp':
            service_type.append(41)
        elif service == 'ntp_u':
            service_type.append(42)
        elif service == 'other':
            service_type.append(43)
        elif service == 'pm_dump':
            service_type.append(44)
        elif service == 'pop_2':
            service_type.append(45)
        elif service == 'pop_3':
            service_type.append(46)
        elif service == 'printer':
            service_type.append(47)
        elif service == 'private':
            service_type.append(48)
        elif service == 'red_i':
            service_type.append(49)
        elif service == 'remote_job':
            service_type.append(50)
        elif service == 'rje':
            service_type.append(51)
        elif service == 'shell':
            service_type.append(52)
        elif service == 'smtp':
            service_type.append(53)
        elif service == 'sql_net':
            service_type.append(54)
        elif service == 'ssh':
            service_type.append(55)
        elif service == 'sunrpc':
            service_type.append(56)
        elif service == 'supdup':
            service_type.append(57)
        elif service == 'systat':
            service_type.append(58)
        elif service == 'telnet':
            service_type.append(59)
        elif service == 'tftp_u':
            service_type.append(60)
        elif service == 'tim_i':
            service_type.append(61)
        elif service == 'time':
            service_type.append(62)
        elif service == 'urh_i':
            service_type.append(63)
        elif service == 'urp_i':
            service_type.append(64)
        elif service == 'uucp':
            service_type.append(65)
        elif service == 'uucp_path':
            service_type.append(66)
        elif service == 'vmnet':
            service_type.append(67)
        elif service == 'whois':
            service_type.append(68)
        elif service == 'X11':
            service_type.append(69)
        elif service == 'Z39_50':
            service_type.append(70)
        else:
            service_type.append(71)

        #set flags
        flag = nsl_Data[i][3].decode('UTF-8')
        if flag == 'OTH':
            flag_type.append(1)
        elif flag == 'REJ':
            flag_type.append(2)
        elif flag == 'RSTO':
            flag_type.append(3)
        elif flag == 'RSTOS0':
            flag_type.append(4)
        elif flag == 'RSTR':
            flag_type.append(5)
        elif flag == 'S0':
            flag_type.append(6)
        elif flag == 'S1':
            flag_type.append(7)
        elif flag == 'S2':
            flag_type.append(8)
        elif flag == 'S3':
            flag_type.append(9)
        elif flag == 'SF':
            flag_type.append(10)
        elif flag == 'OTH':
            flag_type.append(11)
        else:
            flag_type.append(12)

    #And cut out the last column in the kdd_array (or last 2 Cloumns in case attack_type is ussed (includes difficulty level)

    #create final array
    kdd_final = numpy.empty((row_count, 41))
    for i in range(row_count):
        for j in range(41):
            if j == 1:
                kdd_final[i][j] = protocol_type[i]
            elif j == 2:
                kdd_final[i][j] = service_type[i]
            elif j == 3:
                kdd_final[i][j] = flag_type[i]
            else:
                kdd_final[i][j] = nsl_Data[i][j]


    return kdd_final, targets


#Saves data and targets as .npy (numpy) array for later use
#higher saving and loading speed than csv files, but larger files (+80%)
#to the specified location (after scaling). Returns 0 if unsuccessful
def saveDataset(data_filename, data, targets):
    try:
        numpy.save(data_filename, data)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        data = 0
    try:
        numpy.save(data_filename + '_targets', targets)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        targets = 0

    return data, targets


#Tries to load the scaled data
#Returns the data or 0 if unsuccessful
def loadDataset(filename):
    try:
        data = numpy.load(filename + '.npy')
    except:
        data = 0
    try:
        targets = numpy.load(filename + '_targets.npy')
    except:
        print("Unexpected error:", sys.exc_info()[0])
        targets = 0


    return data, targets

#Function for mapping KDD CUP 1999
def KDD_CUPmapper(row_count, kdd_array,class_type='simple'):
    #There attributes are strings and need to be converted in order to be used in a classifier

    protocl_type = []
    service_type = []
    flag_type = []
    targets = []

    for i in range(row_count):  #iterate throug every row and pick an element

        #Create array of targets based on used class
        if class_type == 'simple':
            if kdd_array[i][41].decode('UTF-8') == 'normal.':
                targets.append(0)
            else:
                targets.append(1)

        if class_type == 'simple':
            if kdd_array[i][41].decode('UTF-8') == 'normal.':
                targets.append(0)
            else:
                targets.append(1)



        #set  protocol {tcp,udp,icmp}
        protocol = kdd_array[i][1].decode('UTF-8')
        if protocol == 'tcp':
            protocl_type.append(1)
        elif protocol == 'udp':
            protocl_type.append(2)
        elif protocol == 'icmp':
            protocl_type.append(3)
        else:
            protocl_type.append(4)

        #set service
        service = kdd_array[i][2].decode('UTF-8')
        # {vmnet,smtp,ntp_u,shell,kshell,imap4,netbios_ssn,tftp_u,  = 8, 1-8
        if service == 'vmnet':
            service_type.append(1)
        elif service == 'smtp':
            service_type.append(2)
        elif service == 'ntp_u':
            service_type.append(3)
        elif service == 'shell':
            service_type.append(4)
        elif service == 'kshell':
            service_type.append(5)
        elif service == 'imap4':
            service_type.append(6)
        elif service == 'netbios_ssn':
            service_type.append(7)
        elif service == 'tftp_u':
            service_type.append(8)
        # mtp,uucp,nnsp,echo,tim_i,ssh,iso_tsap,time,netbios_ns,systat, =10, 9 - 18
        elif service == 'mtp':
            service_type.append(9)
        elif service == 'uucp':
            service_type.append(10)
        elif service == 'nnsp':
            service_type.append(11)
        elif service == 'echo':
            service_type.append(12)
        elif service == 'tim_i':
            service_type.append(13)
        elif service == 'ssh':
            service_type.append(14)
        elif service == 'iso_tsap':
            service_type.append(15)
        elif service == 'time':
            service_type.append(16)
        elif service == 'netbios_ns':
            service_type.append(17)
        elif service == 'systat':
            service_type.append(18)
        # hostnames,login,efs,supdup,http_8001,courier,ctf,nntp,ftp_data = 9, 19-27
        elif service == 'hostnames':
            service_type.append(19)
        elif service == 'login':
            service_type.append(20)
        elif service == 'efs':
            service_type.append(21)
        elif service == 'supdup':
            service_type.append(22)
        elif service == 'http_8001':
            service_type.append(23)
        elif service == 'courier':
            service_type.append(24)
        elif service == 'ctf':
            service_type.append(25)
        elif service == 'nntp':
            service_type.append(26)
        elif service == 'ftp_data':
            service_type.append(27)
        # red_i,ldap,http,ftp,pm_dump,exec,klogin,auth,netbios_dgm,other = 10  28-37
        elif service == 'red_i':
            service_type.append(28)
        elif service == 'ldap':
            service_type.append(29)
        elif service == 'http':
            service_type.append(30)
        elif service == 'ftp':
            service_type.append(31)
        elif service == 'pm_dump':
            service_type.append(32)
        elif service == 'exec':
            service_type.append(33)
        elif service == 'klogin':
            service_type.append(34)
        elif service == 'auth':
            service_type.append(35)
        elif service == 'netbios_dgm':
            service_type.append(36)
        elif service == 'other':
            service_type.append(37)
        # link,X11,discard,private,remote_job,IRC,daytime,pop_3,pop_2,gopher = 10, 38-47
        elif service == 'link':
            service_type.append(38)
        elif service == 'X11':
            service_type.append(39)
        elif service == 'discard':
            service_type.append(40)
        elif service == 'private':
            service_type.append(41)
        elif service == 'remote_job':
            service_type.append(42)
        elif service == 'IRC':
            service_type.append(43)
        elif service == 'daytime':
            service_type.append(44)
        elif service == 'pop_3':
            service_type.append(45)
        elif service == 'pop_2':
            service_type.append(46)
        elif service == 'gopher':
            service_type.append(47)
        # sunrpc,name,rje,domain,uucp_path,http_2784,Z39_50,domain_u,csnet_ns, = 9, 48-56
        elif service == 'sunrpc':
            service_type.append(48)
        elif service == 'name':
            service_type.append(49)
        elif service == 'rje':
            service_type.append(50)
        elif service == 'domain':
            service_type.append(51)
        elif service == 'uucp_path':
            service_type.append(52)
        elif service == 'http_2784':
            service_type.append(53)
        elif service == 'Z39_50':
            service_type.append(54)
        elif service == 'domain_u':
            service_type.append(55)
        elif service == 'csnet_ns':
            service_type.append(56)
        # whois,eco_i,bgp,sql_net,printer,telnet,ecr_i,urp_i,netstat,http_443, = 10 = 57-66
        elif service == 'whois':
            service_type.append(57)
        elif service == 'eco_i':
            service_type.append(58)
        elif service == 'bgp':
            service_type.append(59)
        elif service == 'sql_net':
            service_type.append(60)
        elif service == 'printer':
            service_type.append(61)
        elif service == 'telnet':
            service_type.append(62)
        elif service == 'ecr_i':
            service_type.append(63)
        elif service == 'urp_i':
            service_type.append(64)
        elif service == 'netstat':
            service_type.append(65)
        elif service == 'http_443':
            service_type.append(66)
        # harvest,aol,urh_i,finger} = 4, 67-70
        elif service == 'harvest':
            service_type.append(67)
        elif service == 'aol':
            service_type.append(68)
        elif service == 'urh_i':
            service_type.append(69)
        elif service == 'finger':
            service_type.append(70)
        else:
            service_type.append(71)#normally, this shouldn't exist


        #set flags
        #@attribute flag {RSTR,S3,SF,RSTO,SH,OTH,S2,RSTOS0,S1,S0,REJ}
        flag = kdd_array[i][3].decode('UTF-8')
        if flag == 'RSTR':
            flag_type.append(1)
        elif flag == 'S3':
            flag_type.append(2)
        elif flag == 'SF':
            flag_type.append(3)
        elif flag == 'RSTO':
            flag_type.append(4)
        elif flag == 'SH':
            flag_type.append(5)
        elif flag == 'OTH':
            flag_type.append(6)
        elif flag == 'S2':
            flag_type.append(7)
        elif flag == 'RSTOS0':
            flag_type.append(8)
        elif flag == 'S1':
            flag_type.append(9)
        elif flag == 'S0':
            flag_type.append(10)
        elif flag == 'REJ':
            flag_type.append(11)
        else:
            flag_type.append(12)#normally, this shouldn't exist


    #And cut out the last column in the kdd_array

    #create final array
    kdd_final = numpy.empty((row_count, 41))
    for i in range(row_count):
        for j in range(41):
            if j == 1:
                kdd_final[i][j] = protocl_type[i]
            elif j == 2:
                kdd_final[i][j] = service_type[i]
            elif j == 3:
                kdd_final[i][j] = flag_type[i]
            else:
                kdd_final[i][j] = kdd_array[i][j]

    return kdd_final, targets

#Function for mapping KDD CUP 1999
def mapCUP_KDDFile(filename, save=False,class_type='simple'):
    kdd_data = numpy.genfromtxt(filename, delimiter=',', dtype=None)
    kdd_data, targets = KDD_CUPmapper(len(kdd_data), kdd_data,class_type)

    #Write the file for further usage
    if save == True:
        with open(filename + '.csv', 'w', newline='') as fp:
            a = csv.writer(fp, delimiter=',')
            a.writerows(kdd_data)
        numpy.savetxt(filename + '.targets.csv', targets, delimiter=",",fmt ='%1d')

    return kdd_data, targets


#creates a KDD-Subdataset only with the defined classes
def makeDataset(data,targets,classes):

    #determine the size of the new dataset
    row_count = 0

    for item in classes:
        row_count = row_count + targets.count(item)

    #create final array
    new_dataset = numpy.empty((row_count, len(data[0])))
    new_targets = []
    x=0

    #go through all the data
    for i in range(len(data)):
        #if the target matches at that place
        for item in classes:
            #copy it to the new dataset
            if item == targets[i]:
                new_dataset[x] = data[i]
                new_targets.append(targets[i])
                x = x + 1


    return new_dataset,new_targets

#Finds the specified intrusion type of the regular NSL-KDD testset (no randomization allowed!)
def findIntrusionType(targets,name='test'):

    if name == 'test':
        targets_typed = numpy.genfromtxt('data/NSL/NSL_AttackTypes_test.csv', delimiter=',')
    elif name == 'train':
        targets_typed = numpy.genfromtxt('data/NSL/NSL_AttackTypes_train.csv', delimiter=',')
    newTargets = []
    for i in range(len(targets)):
        if targets[i] == 1 and targets_typed[i] >= 1:
            newTargets.append(targets_typed[i])
        else:
            newTargets.append(targets[i])

    #return the true types and the found ones, mapped to the attack type

    print(metrics.confusion_matrix(targets_typed, newTargets))
    return newTargets,targets_typed
