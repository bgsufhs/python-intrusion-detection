__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np
from sklearn import ensemble, tree, neighbors, neural_network,naive_bayes,svm

k = 10 #Nr of folds


#Specify dataset names
full_filename = 'data/CUP/KDD_Cup_10.arff'

#map the datasets
data, dataTargets = KDDTools.mapCUP_KDDFile(full_filename,False)
#scale data
data, data = ClfTools.scaleAtan(data)

#sample the data
#testdata does not exist with testsize=0!
fullData,fullTargets,testData,testTargets = ClfTools.randomSampling(data,dataTargets,testsize=0)




for i in range (1,k):


    trainData, trainTargets,  testData, testTargets=ClfTools.splitSets(k,i,fullData,fullTargets)



    clf_boost = ensemble.AdaBoostClassifier()
    clf_boost.fit(trainData, trainTargets)

    clf_dTree = tree.DecisionTreeClassifier()
    clf_dTree.fit(trainData, trainTargets)

    clf_nn = neighbors.NearestCentroid()
    clf_nn.fit(trainData, trainTargets)

    clf_svm = svm.SVC(kernel='rbf')
    clf_svm.fit(trainData, trainTargets)


    boost_out = clf_boost.predict(testData)
    dTree_out = clf_dTree.predict(testData)
    nn_out = clf_nn.predict(testData)
    svm_out = clf_svm.predict(testData)

    ClfTools.printStats(testTargets,boost_out,'Boosting')
    ClfTools.printStats(testTargets,dTree_out,'Decision Tree')
    ClfTools.printStats(testTargets,nn_out,'Nearest Centroid')
    ClfTools.printStats(testTargets,svm_out,'SVM')

    #generate voted output
    votes = []

    #Go through every vote:
    for i in range(0,len(testTargets)):
        if (dTree_out[i] == 0) or ((svm_out[i] == 0) and (boost_out[i] == 0)):
            votes.append(0)
        elif (dTree_out[i] == 1) and (boost_out[i] == 1):
            votes.append(1)
        elif (boost_out[i] == 1) and (svm_out[i] == 1):
            votes.append(1)
        else:
            votes.append(dTree_out[i])


    ClfTools.printStats(testTargets,votes,'Vote-Classifier')

