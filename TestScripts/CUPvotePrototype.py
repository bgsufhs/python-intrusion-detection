__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np
from sklearn import ensemble, tree, neighbors, neural_network,naive_bayes,svm


testdata_filename = 'data/CUP/testdata_scaled'
traindata_filename = 'data/CUP/traindata_scaled'

trainData, trainTargets = KDDTools.loadDataset(traindata_filename)
testData, testTargets = KDDTools.loadDataset(testdata_filename)

clf_boost = ClfTools.loadPersistent('persistent/CUP/Boosting')
clf_dTree = ClfTools.loadPersistent('persistent/CUP/DTree')
clf_nn = ClfTools.loadPersistent('persistent/CUP/NCent')
clf_svm = ClfTools.loadPersistent('persistent/CUP/SVM')

if clf_boost == 0:
    print('No data for boosting found, training classifier...')
    clf_boost = ensemble.AdaBoostClassifier()
    clf_boost.fit(trainData, trainTargets)
    KDDTools.createPersistent(clf_boost, 'persistent/CUP/Boosting')
if clf_dTree == 0:
    print('No data for decision tree found, training classifier...')
    clf_dTree = tree.DecisionTreeClassifier()
    clf_dTree.fit(trainData, trainTargets)
    KDDTools.createPersistent(clf_dTree,'persistent/CUP/DTree')
if clf_nn == 0:
    print('No data for nearest centroid found, training classifier...')
    clf_nn = neighbors.NearestCentroid()
    clf_nn.fit(trainData, trainTargets)
    KDDTools.createPersistent(clf_nn,'persistent/CUP/NCent')
if clf_svm == 0:
    print('No data for SVM found, training classifier...')
    clf_svm = svm.SVC(kernel='rbf')
    clf_svm.fit(trainData, trainTargets)
    KDDTools.createPersistent(clf_svm,'persistent/CUP/SVM')

boost_out = clf_boost.predict(testData)
dTree_out = clf_dTree.predict(testData)
nn_out = clf_nn.predict(testData)
svm_out = clf_svm.predict(testData)

ClfTools.printStats(testTargets,boost_out,'Boosting')
ClfTools.printStats(testTargets,dTree_out,'Decision Tree')
ClfTools.printStats(testTargets,nn_out,'Nearest Centroid')
ClfTools.printStats(testTargets,svm_out,'SVM')

#generate voted output
votes = []

#Go through every vote:
for i in range(0,len(testTargets)):
    if (dTree_out[i] == 0) or ((svm_out[i] == 0) and (boost_out[i] == 0)):
        votes.append(0)
    elif (dTree_out[i] == 1) and (boost_out[i] == 1):
        votes.append(1)
    elif (boost_out[i] == 1) and (nn_out[i] == 1):
        votes.append(1)
    else:
        votes.append(dTree_out[i])



ClfTools.printStats(testTargets,votes,'Vote-Classifier')