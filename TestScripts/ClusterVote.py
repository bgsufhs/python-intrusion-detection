__author__ = 'jorisl'
# Specify dataset names
import KDDTools
import ClfTools
from sklearn import clone
from sklearn import svm

training_filename = 'data/NSL/Unmod_KDD_NSL_train.arff'
test_filename = 'data/NSL/Unmod_KDD_NSL_test.arff'


#map the datasets
trainData, trainTargets = KDDTools.genFromNSL(training_filename)
testData, testTargets = KDDTools.genFromNSL(test_filename)

trainData, testData = ClfTools.scaleAtan(trainData,testData)
trainData,trainTargets = KDDTools.makeDataset(trainData,trainTargets,[0])


#Model specification with  parameters
clf = svm.OneClassSVM(nu=0.1, kernel="rbf", gamma=0.1)
print(len(trainData))
clf.fit(trainData)
ClfTools.printStats(testTargets,clf.predict(testData))