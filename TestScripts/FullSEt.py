

#Datasets with 4 Attack types
training_filename = 'data/NSL4/KDDTrain+.arff'
test_filename = 'data/NSL4/KDDTest+.arff'

#map the datasets
trainData, trainTargets = KDDTools.genFromNSL(training_filename, True, True)
testData, testTargets = KDDTools.genFromNSL(test_filename, True, True)

#Use the full Dataset
fullData = []
fullTargets = []
for i in range(len(trainData)):
    fullData.append(trainData[i])
    fullTargets.append(trainTargets[i])
for j in range(len(testData)):
    fullData.append(trainData[j])
    fullTargets.append(trainTargets[j])


#Scale EVERYTHING the same way! (in case it is propagated, the same scaling must be applied!)
fullData, dummy = ClfTools.scaleSimple(fullData)
#Randomly mix the data
newOrder = np.arange(len(fullData))
np.random.shuffle(newOrder)
fullData = [fullData[i] for i in newOrder]
fullTargets = [fullTargets[i] for i in newOrder]



# HAN-specific IDS Data: DDos(2),Probe(3),Normal(0),
HAN_fullData,HAN_fullTargets = KDDTools.makeDataset(fullData,fullTargets,[0,2,3])
#Convert into 2-Class problem
HAN_fullTargets = replaceTargets(HAN_fullTargets)
#Split into 70% training and 30%Testing data
HAN_trainData,HAN_trainTargets,HAN_testData,HAN_testTargets = ClfTools.randomSampling(HAN_fullData, HAN_fullTargets,0.3)
#Create a 10% weight-calibration set and a 10% test set
HAN_calibrationData,HAN_calibrationTargets,HAN_testData,HAN_testTargets = ClfTools.randomSampling(HAN_testData, HAN_testTargets,0.5)
#Save the test Data
KDDTools.saveDataset('data/SGDIDS/HAN_testset',HAN_testData,HAN_testTargets)


# NAN-specific IDS Data: DDos(2),Probe(3),Normal(0),U2R(4)
NAN_fullData,NAN_fullTargets = KDDTools.makeDataset(fullData,fullTargets,[0,2,3,4])
NAN_fullTargets = replaceTargets(NAN_fullTargets)
NAN_trainData,NAN_trainTargets,NAN_testData,NAN_testTargets = ClfTools.randomSampling(NAN_fullData, NAN_fullTargets,0.3)
NAN_calibrationData,NAN_calibrationTargets,NAN_testData,NAN_testTargets = ClfTools.randomSampling(NAN_testData, NAN_testTargets,0.5)
KDDTools.saveDataset('data/SGDIDS/NAN_testset',NAN_testData,NAN_testTargets)


# WAN-specific IDS Data: Use full set
WAN_trainData,WAN_trainTargets,testData,testTargets = ClfTools.randomSampling(fullData, replaceTargets(fullTargets),0.999)
WAN_calibrationData,WAN_calibrationTargets,WAN_testData,WAN_testTargets = ClfTools.randomSampling(testData, testTargets,0.5)
KDDTools.saveDataset('data/SGDIDS/WAN_testset',NAN_testData,NAN_testTargets)__author__ = 'jorisl'
