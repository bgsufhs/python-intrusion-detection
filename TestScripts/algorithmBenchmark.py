__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
from sklearn.ensemble import (RandomForestClassifier, ExtraTreesClassifier,AdaBoostClassifier)
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import clone
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB

# Parameters
n_classes = 2
n_estimators = 5


testdata_filename = 'data/NSL/testdata'
traindata_filename = 'data/NSL/traindata'



trainData, trainTargets = KDDTools.loadDataset(traindata_filename)
testData, testTargets = KDDTools.loadDataset(testdata_filename)


models = [DecisionTreeClassifier(max_depth=None,class_weight='auto',min_samples_leaf=20),
          AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70),
          KNeighborsClassifier(n_neighbors=1,weights='distance',algorithm='kd_tree',leaf_size=50),
          SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, cache_size=500, verbose=True)]



strings = ['DecisionTree','AdaBoost','KNeighbors','SVM']
i=0
for model in models:
    clf = clone(model)
    clf.fit(trainData, trainTargets)
    out = clf.predict(testData)

    ClfTools.printStats(testTargets,out,strings[i])
    i=i+1
