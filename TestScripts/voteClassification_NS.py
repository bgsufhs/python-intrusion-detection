__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np
import math
from sklearn.ensemble import (RandomForestClassifier, ExtraTreesClassifier,AdaBoostClassifier)
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import clone
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB

def calculateWeight(precision):
    return 1+ 2**(precision*5)+precision/150000*math.exp(precision/0.06)

# Parameters
n_classes = 2
n_estimators = 5


testdata_filename = 'data/NSL/testdata_own'
traindata_filename = 'data/NSL/traindata_own'
verifdata_filename = 'data/NSL/verifdata_own'



trainData, trainTargets = KDDTools.loadDataset(traindata_filename)
testData, testTargets = KDDTools.loadDataset(testdata_filename)
verifData, verifTargets = KDDTools.loadDataset(verifdata_filename)



models = [DecisionTreeClassifier(max_depth=None,class_weight='auto',min_samples_leaf=20),
          AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70),
          KNeighborsClassifier(n_neighbors=1,weights='distance',algorithm='kd_tree',leaf_size=50),
          SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, cache_size=500, verbose=True)]
model_weights_attack = [] #Weights are calbirated after test set
model_weights_normal = []
persistent_models = []

strings = ['DecisionTree','AdaBoost', 'KNeighbours', 'SVM']
i=0
j=0
x=0
votes = []
weights = []

for j in range(0,len(testTargets)):
    weights.append(np.array([0,0]).astype(float))

for model in models:
    clf = clone(model)
    clf.fit(trainData, trainTargets)
    persistent_models.append(clf)
    votes.append(clf.predict(testData)) #string with predictions
    perfs = ClfTools.getPerformance(testTargets,votes[i]) # return true_pr,true_nr,false_pr,false_nr

    model_weights_normal.append(calculateWeight(perfs[1]))
    model_weights_attack.append(calculateWeight(perfs[0]))
    print( model_weights_normal[i])
    print( model_weights_attack[i])
    j=0
    for cl in votes[i]:   #go through every vote
        if cl == 0:
            weights[j][0] = weights[j][0] + model_weights_normal[i] #and add the weight to the matrix for the specific vote
        else:
            weights[j][1] = weights[j][1] + model_weights_attack[i] #and add the weight to the matrix for the specific vote
        j=j+1

    ClfTools.printStats(testTargets,votes[i],strings[i])
    i=i+1


v_clf = []


for x in range(0,len(testTargets)):
    if weights[x][0] >= weights[x][1]:  #if vote is bigger or equal FOR normal, use normal...
        v_clf.append(0)
    else:
        v_clf.append(1)

ClfTools.printStats(testTargets,v_clf,'VOTE')


#verifData, verifTargets
i=0
j=0
x=0
votes = []
weights = []
for j in range(0,len(verifData)):
    weights.append(np.array([0,0]).astype(float))

for clf in persistent_models:
    votes.append(clf.predict(verifData)) #string with predictions

    j=0
    for cl in votes[i]:   #go through every vote
        if cl == 0:
            weights[j][0] = weights[j][0] + model_weights_normal[i] #and add the weight to the matrix for the specific vote
        else:
            weights[j][1] = weights[j][1] + model_weights_attack[i] #and add the weight to the matrix for the specific vote
        j=j+1

    ClfTools.printStats(verifTargets,votes[i],strings[i])
    i=i+1

v_clf = []

for x in range(0,len(verifTargets)):
    if weights[x][0] >= weights[x][1]:  #if vote is bigger or equal FOR normal, use normal...
        v_clf.append(0)
    else:
        v_clf.append(1)

ClfTools.printStats(verifTargets,v_clf,'VOTE')
