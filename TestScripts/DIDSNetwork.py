__author__ = 'Joris Lueckenga'
import ClfTools
import KDDTools

#Testscript for DIDS_Node Class

#Specify dataset names
full_filename = 'data/CUP/KDD_Cup_10.arff'

#map the datasets
data, dataTargets = KDDTools.mapCUP_KDDFile(full_filename,False)
#scale data
data, data = ClfTools.scaleAtan(data)
k=10
#sample the data
#testdata does not exist with testsize=0!
fullData,fullTargets,testData,testTargets = ClfTools.randomSampling(data,dataTargets,testsize=0)
trainData, trainTargets,  testData, testTargets=ClfTools.splitSets(k,1,fullData,fullTargets)

print('what did not work?')
WAN_Node = ClfTools.DIDS_Node('HAN',trainData,trainTargets)
WAN_Node.train_node()
WAN_Node.predict(testData,testTargets)
