__author__ = 'Joris Lueckenga'
import KDDTools
import numpy
import ClfTools
from sklearn.cross_validation import train_test_split

#Specify dataset names
full_filename = 'data/CUP/KDD_Cup_10.arff'

#map the datasets
data, dataTargets = KDDTools.mapCUP_KDDFile(full_filename,False)
print(dataTargets)
data, data = ClfTools.scaleAtan(data)


data_filename = 'data/CUP/data_scaled'

train_data,train_targets,test_data,test_targets = ClfTools.randomSampling(data,dataTargets)

#Save the scaled datasets
KDDTools.saveDataset('data/CUP/testdata_scaled', test_data, test_targets)
KDDTools.saveDataset('data/CUP/traindata_scaled', train_data, train_targets)


