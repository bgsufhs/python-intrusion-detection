__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np

# Specify dataset names
training_filename = 'data/NSL/Unmod_KDD_NSL_train.arff'
test_filename = 'data/NSL/Unmod_KDD_NSL_test.arff'


#map the datasets
trainData, trainTargets = KDDTools.genFromNSL(training_filename)
testData, testTargets = KDDTools.genFromNSL(test_filename)

trainData, testData = ClfTools.scaleAtan(trainData,testData)

KDDTools.saveDataset('data/NSL/traindata', trainData, trainTargets)
KDDTools.saveDataset('data/NSL/testdata', testData, testTargets)




