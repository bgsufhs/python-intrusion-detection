__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np
import time
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import clone
from sklearn.svm import SVC
from sklearn import metrics

def calculateWeightFPR(precision):
    FPRweight = 1/((1-precision)+0.01)
    print('FPR-Weigth: ' +str(FPRweight))
    return FPRweight

def calculateWeightFNR(precision):
    FNRweight = 1/((1-precision)+0.01)
    print('FNR-Weigth: ' +str(FNRweight))
    return FNRweight

#Definition of weight calculation functions
def calculateWeight(precision,A,B):
    FPRweight = A/((1-precision*B)+0.01)
    return FPRweight

# Define and load datasets
testdata_filename = 'data/NSL/testdata'
traindata_filename = 'data/NSL/traindata'
verifdata_filename = 'data/NSL/verifdata'

trainData, trainTargets = KDDTools.loadDataset(traindata_filename)
testData, testTargets = KDDTools.loadDataset(testdata_filename)
verifData, verifTargets = KDDTools.loadDataset(verifdata_filename)

#Model specification with  parameters
models = [DecisionTreeClassifier(max_depth=None,class_weight='auto',min_samples_leaf=20),
          AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70),
          KNeighborsClassifier(n_neighbors=1,weights='distance',algorithm='kd_tree',leaf_size=50),
          SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, cache_size=500, verbose=True)]

#Define weight array for calibrating weights with test set
model_weights_normal = []
model_weights_attack = []

#Persisten model definition to avoid training twice
persistent_models = []

#String and variable definition for iterations
strings = ['DecisionTree','AdaBoost', 'KNeighbours', 'SVM']
i=j=x=0

#Array for predicted targets storage
votes = []

#Create matrix to store assigned weights

#Iterate through each model, train, predict and calibrate and calculate the weight
for model in models:
    clf = clone(model)
    clf = ClfTools.loadPersistent('persistent/NSL/' + strings[i])
    votes.append(clf.predict(testData)) #string with predictions
    perfs = ClfTools.getPerformance(testTargets,votes[i])
    #Print the classifier performance


    ClfTools.printStats(testTargets,votes[i],strings[i])
    i=i+1

#Try best parameters for the votin system
ClfTools.getBestParams(votes,testTargets,[0.01],[1])
ClfTools.findFormulaParams(votes,testTargets)

