__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np

# Specify dataset names
training_filename = 'data/NSL/Unmod_KDD_NSL_train.arff'
test_filename = 'data/NSL/Unmod_KDD_NSL_test.arff'


#map the datasets
trainData, trainTargets = KDDTools.genFromNSL(training_filename)
testData, testTargets = KDDTools.genFromNSL(test_filename)

#create a full set
fullData = []
fullTargets = []
for i in range(len(trainData)):
    fullData.append(trainData[i])
    fullTargets.append(trainTargets[i])
for j in range(len(testData)):
    fullData.append(trainData[j])
    fullTargets.append(trainTargets[j])


trainData, testData = ClfTools.scaleAtan(trainData,testData)
testData,testTargets,verifData,verifTargets = ClfTools.randomSampling(testData, testTargets,0.5)

#Save the scaled datasets
KDDTools.saveDataset('data/NSL/traindata', trainData, trainTargets)
KDDTools.saveDataset('data/NSL/testdata', testData, testTargets)
KDDTools.saveDataset('data/NSL/verifdata', verifData, verifTargets)


#create own sets with full dataset
trainData,trainTargets,testData,testTargets = ClfTools.randomSampling(fullData, fullTargets,0.2)
trainData, testData = ClfTools.scaleAtan(trainData,testData)
testData,testTargets,verifData,verifTargets = ClfTools.randomSampling(testData, testTargets,0.25) #1/4 verification Data
#Save the scaled datasets
KDDTools.saveDataset('data/NSL/traindata_own', trainData, trainTargets)
KDDTools.saveDataset('data/NSL/testdata_own', testData, testTargets)
KDDTools.saveDataset('data/NSL/verifdata_own', verifData, verifTargets)


