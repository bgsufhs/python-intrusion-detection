__author__ = 'jorisl'
import KDDTools
import ClfTools
import numpy as np
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

testData, testTargets = KDDTools.loadDataset('data/SGDIDS/HAN_testset')

#Get trained models
HAN_AdaBoost = AdaBoostClassifier()
HAN_AdaBoost = ClfTools.loadPersistent('persistent/SGDIDS/HAN/AdaBoost')
HAN_SVM = SVC()
HAN_SVM = ClfTools.loadPersistent('persistent/SGDIDS/HAN/SVM')

NAN_AdaBoost = AdaBoostClassifier()
NAN_AdaBoost = ClfTools.loadPersistent('persistent/SGDIDS/NAN/AdaBoost')
NAN_DecisionTree = DecisionTreeClassifier()
NAN_DecisionTree = ClfTools.loadPersistent('persistent/SGDIDS/NAN/DecisionTree')
NAN_SVM = SVC()
NAN_SVM = ClfTools.loadPersistent('persistent/SGDIDS/NAN/SVM')

WAN_AdaBoost = AdaBoostClassifier()
WAN_AdaBoost = ClfTools.loadPersistent('persistent/SGDIDS/WAN/AdaBoost')
WAN_DecisionTree = DecisionTreeClassifier()
WAN_DecisionTree = ClfTools.loadPersistent('persistent/SGDIDS/WAN/DecisionTree')
WAN_KNeighbour = KNeighborsClassifier()
WAN_KNeighbour = ClfTools.loadPersistent('persistent/SGDIDS/WAN/KNeighbour')
WAN_SVM = SVC()
WAN_SVM = ClfTools.loadPersistent('persistent/SGDIDS/WAN/SVM')

#Get Weights
attack_weights = np.loadtxt('persistent/SGDIDS/WAN/WAN_weights_attack.txt')
normal_weights = np.loadtxt('persistent/SGDIDS/WAN/WAN_weights_normal.txt')


#######################################################################################################################
##########################################        HAN-VOTE     ########################################################
#######################################################################################################################
#Do HAN-Testset testing (max 91%)
#go Through each Datap
SGDIDS_Vote = []
sendToNan=0
sendToWan=0
for data_instance in testData:
    #Classify with HAN
    Decision = 0
    HAN_Vote = [HAN_AdaBoost.predict(data_instance) ,HAN_SVM.predict(data_instance)]
    #Both votes have to match! if that's not tha case;
    if HAN_Vote[0][0] != HAN_Vote[1][0]:
        sendToNan=sendToNan+1
        NAN_Vote = [NAN_AdaBoost.predict(data_instance), NAN_DecisionTree.predict(data_instance),
                    NAN_SVM.predict(data_instance)]
        #At least SVM and one other must match
        if (NAN_Vote[2][0] != NAN_Vote[1][0]) or (NAN_Vote[2][0] != NAN_Vote[0][0]):
            sendToWan=sendToWan+1
            #Same order required for weights!
            WAN_Vote = [WAN_DecisionTree.predict(data_instance),WAN_AdaBoost.predict(data_instance),
                       WAN_KNeighbour.predict(data_instance),WAN_SVM.predict(data_instance)]
            pos = 0
            Vote0=0
            Vote1=1
            for pos in range(len(WAN_Vote)):
                if WAN_Vote[pos][0] == 1:
                    Vote1 = Vote1 + attack_weights[pos]
                else:
                    Vote0 = Vote0 + normal_weights[pos]
            if Vote0>=Vote1:
                Decision=0
            else:
                Decision=1
        else:
            Decision = NAN_Vote[2][0]
    else:
        Decision = HAN_Vote[0][0]
    SGDIDS_Vote.append(Decision)


ClfTools.printStats(testTargets,SGDIDS_Vote,'VOTE_HAN')
print('Number of packets send to NAN:' + str(sendToNan) + ' of ' + str(len(testTargets)))
print('Number of packets send to WAN:' + str(sendToWan) + ' of ' + str(len(testTargets)))

#######################################################################################################################
##########################################        NAN-VOTE     ########################################################
#######################################################################################################################

testData, testTargets = KDDTools.loadDataset('data/SGDIDS/NAN_testset')
sendToWan=0

SGDIDS_Vote = []
for data_instance in testData:
    #Classify with NAN
    Decision = 0
    NAN_Vote = [NAN_AdaBoost.predict(data_instance), NAN_DecisionTree.predict(data_instance),
                    NAN_SVM.predict(data_instance)]
    #Both votes have to match! if that's not tha case;
    if (NAN_Vote[2][0] != NAN_Vote[1][0]) or (NAN_Vote[2][0] != NAN_Vote[0][0]):
        sendToWan=sendToWan+1
        #Same order required for weights!
        WAN_Vote = [WAN_DecisionTree.predict(data_instance),WAN_AdaBoost.predict(data_instance),
                    WAN_KNeighbour.predict(data_instance),WAN_SVM.predict(data_instance)]
        pos = 0
        Vote0=0
        Vote1=1
        for pos in range(len(WAN_Vote)):
            if WAN_Vote[pos][0] == 1:
                Vote1 = Vote1 + attack_weights[pos]
            else:
                Vote0 = Vote0 + normal_weights[pos]
        if Vote0>=Vote1:
            Decision=0
        else:
            Decision=1
    else:
        Decision = NAN_Vote[0][0]

    SGDIDS_Vote.append(Decision)


#always those arrays...
ClfTools.printStats(testTargets,SGDIDS_Vote,'VOTE_HAN')
print('Number of packets send to WAN:' + str(sendToWan) + ' of ' + str(len(testTargets)))