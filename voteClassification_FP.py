__author__ = 'Joris Lueckenga'
import KDDTools
import ClfTools
import numpy as np
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import clone
from sklearn.svm import SVC
from sklearn import metrics

#Definition of weight calculation functions
def calculateWeightFPR(precision,FP):
    FPRweight = (1+(1/FP))/((1-precision)+0.01)
    print('FPR-Weigth: ' +str(FPRweight))
    return FPRweight

def calculateWeightFNR(precision,FP):
    FNRweight = (1+(1/FP))/((1-precision)+0.01)
    print('FNR-Weigth: ' +str(FNRweight))
    return FNRweight

# Define and load datasets
testdata_filename = 'data/NSL/testdata'
traindata_filename = 'data/NSL/traindata'
verifdata_filename = 'data/NSL/verifdata'

trainData, trainTargets = KDDTools.loadDataset(traindata_filename)
testData, testTargets = KDDTools.loadDataset(testdata_filename)
verifData, verifTargets = KDDTools.loadDataset(verifdata_filename)

#Model specification with  parameters
models = [DecisionTreeClassifier(max_depth=None,class_weight='auto',min_samples_leaf=20),
          AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70),
          KNeighborsClassifier(n_neighbors=1,weights='distance',algorithm='kd_tree',leaf_size=50),
          SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, cache_size=500, verbose=True)]

#Define weight array for calibrating weights with test set
model_weights_attack = []
model_weights_normal = []

#Persisten model definition to avoid training twice
persistent_models = []

#String and variable definition for iterations
strings = ['DecisionTree','AdaBoost', 'KNeighbours', 'SVM']
i=j=x=0

#Array for predicted targets storage
votes = []

#Create matrix to store assigned weights
weights = []
for j in range(0,len(testTargets)):
    weights.append(np.array([0,0]).astype(float))

#Iterate through each model, train, predict and calibrate and calculate the weight
for model in models:
    clf = clone(model)
    clf.fit(trainData, trainTargets)
    persistent_models.append(clf)
    clf = ClfTools.createPersistent(clf,'persistent/NSL/' + strings[i])
    votes.append(clf.predict(testData)) #string with predictions

    #Calibrate weights
    perfs = ClfTools.getPerformance(testTargets,votes[i]) # return true_pr,true_nr,false_pr,false_nr
    model_weights_normal.append(calculateWeightFNR(perfs[1],perfs[2]))
    model_weights_attack.append(calculateWeightFPR(perfs[0],perfs[2]))

    #Add weights to matrix
    j=0
    for cl in votes[i]:   #go through every vote
        if cl == 0:
            weights[j][0] = weights[j][0] + model_weights_normal[i] #and add the weight to the matrix for the specific vote
        else:
            weights[j][1] = weights[j][1] + model_weights_attack[i] #and add the weight to the matrix for the specific vote
        j=j+1

    #Print the classifier performance
    ClfTools.printStats(testTargets,votes[i],strings[i])
    #KDDTools.findIntrusionType(votes[i])


    i=i+1


#Execute Vote-Classification & print stats
vote_clf = []
for x in range(0,len(testTargets)):
    if weights[x][0] >= weights[x][1]:  #if vote is bigger or equal FOR normal, use normal...
        vote_clf.append(0)
    else:
        vote_clf.append(1)

ClfTools.printStats(testTargets,vote_clf,'VOTE')