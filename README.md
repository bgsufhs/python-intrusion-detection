# README #

To execute the scripts, following requirements have to be met:
-  [Pyhton Version 3.4](https://www.python.org/download/releases/3.4.0/)
-  [Scikit-Learn 0.15](http://scikit-learn.org/stable/)
-  [Numpy 1.9.2](www.numpy.org)
-  [Scipy 0.15.1](www.scipy.org/)

### What is this repository for? ###

This repository is used to test a classifier voting mechanism.
The KDD-NSL and KDD-Cup 1999 Datasets are the provided features to train and test different types of classifiers. It has to be evaluated, if a voting scenario can be used, to increased classifier performance (with special interest in reducing false positives).

### How do I get set up? ###

Download required librarys & execute the script.
All required data, e.g the dataset-files have to be in the correct location in order to execute the code successfully.

### Who do I talk to? ###

Joris Lueckenga - jorisl@bgsu.edu / joris.lueckenga@gmail.com