__author__ = 'Joris Lueckenga'
from sklearn import metrics, preprocessing
from sklearn import ensemble, tree, svm
import KDDTools
import numpy as np
import pickle

# Prints the results for a specific classifier
def printStats(targets, predicted, name='classifier'):
    rates = metrics.confusion_matrix(targets, predicted)

    size = len(targets)
    tn = rates[0][0]
    fp = rates[0][1]
    tp = rates[1][1]
    fn = rates[1][0]
    print('Achieved values for', name, ':')
    print('True positives:', tp, '\t (', np.round(tp / (tp + fp), decimals=5)*100, '%)')
    print('True negatives:', tn, '\t (', np.round(tn / (tn + fn), decimals=5)*100, '%)')
    print('False positives:', fp, '\t (', np.round(fp / (tp + fp), decimals=5)*100, '%)')
    print('False negatives:', fn, '\t (', np.round(fn / (tn + fn), decimals=5)*100, '%)')
    print('Overall accuracy:', np.round((tp + tn) / size, decimals=5)*100, '%')

#Output of performance rates, e.g for weight calculation
def getPerformance(targets, predicted):
    rates = metrics.confusion_matrix(targets, predicted)
    tn = rates[0][0]
    fp = rates[0][1]
    tp = rates[1][1]
    fn = rates[1][0]

    true_p = np.round(tp / (tp + fp), decimals=5)
    true_n = np.round(tn / (tn + fn), decimals=5)
    false_p = np.round(fp / (tp + tp), decimals=5)
    false_n =  np.round(fn / (tn + fn), decimals=5)
    accuracy = (tn+tp)/(tn+tp+fn+fp)

    return true_p,true_n,false_p,false_n,accuracy


#Very useful scaling method (should always be tested, usually improves classification)
#Scikit does not use Atan-Scaling afaik.
def scaleAtan(trainData, testData=0):
    #Create scikit scaler
    std_scale = preprocessing.StandardScaler(copy=True)

    #Perform Z-Score scaling
    std_scale.fit(trainData)
    trainData = std_scale.fit_transform(trainData)
    if testData is not 0:
        testData = std_scale.fit_transform(testData)

    #Perform atan-Scaling
    trainData = 2 / np.pi * np.arctan(trainData)
    if testData is not 0:
        testData = 2 / np.pi * np.arctan(testData)
    else:
        testData=trainData

    return trainData, testData

def scaleSimple(trainData, testData=0):
    #Create scikit scaler
    std_scale = preprocessing.StandardScaler(copy=True)
    range_scale = preprocessing.MinMaxScaler(feature_range=(-1, 1))

    #Perform Z-Score scaling
    std_scale.fit(trainData)
    trainData = std_scale.fit_transform(trainData)
    trainData = range_scale.fit_transform(trainData) #scales to range 0-1

    if testData != 0:
        testData = std_scale.fit_transform(testData)
        testData = range_scale.fit_transform(testData)


    return trainData, testData

#Trys to load a persistent model
#Returns the trained classifier or 0 if unsuccessful
def loadPersistent(savename):
    #Load trained model
    savename = savename + '.pickle'
    try:
        with open(savename, 'rb') as f:
            clf = pickle.load(f)
    except:
        clf = 0

    return clf


#Create a model file after successful training
#Data is saved as .pickle file
#Returns the trained classifier and 0 if unsuccessful
def createPersistent(classifier_trained, savename):
    savename = savename + '.pickle'
    try:
        with open(savename, 'wb') as f:
            pickle.dump(classifier_trained, f)
    except:
        print("Unexpected error:", sys.exc_info()[0])
        classifier_trained = 0
    return classifier_trained


#Shuffles the test data randomly and creates testing/training datasets
def randomSampling(data,targets,testsize=0.2):

    #Create an array of the data size from 0 to X
    newOrder = np.arange(len(targets))

    #shuffle the array
    np.random.shuffle(newOrder)

    #Use the array to re-arrange the data and targets lists
    data = [data[i] for i in newOrder]
    targets = [targets[i] for i in newOrder]

    #now split the data and targets equally
    splitsize = np.floor(testsize*len(targets))
    splitsize = int(splitsize)

    #Use the first part for the test data..
    split_test = np.arange(splitsize)
    split_train = np.arange(splitsize+1,len(targets))
    split_test.astype(int)
    split_train.astype(int)


    test_data = [data[i] for i in split_test]
    test_targets = [targets[i] for i in split_test]
    train_data = [data[i] for i in split_train]
    train_targets = [targets[i] for i in split_train]

    return train_data,train_targets,test_data,test_targets

#Split data for k-folds
def splitSets(folds,foldnumber,data,targets):

    len_data = len(data) #get the length of the dataset
    foldsize = np.floor(len_data/folds)   #get the size of the fold

    #go to beginning of the fold by: foldsize*foldnumber. End is foldsize added
    test_split = np.arange(foldsize*foldnumber,foldsize*foldnumber+foldsize)

    #get the rest of the data in the training set
    train_split = np.arange(0,foldsize)
    np.append(train_split,np.arange(foldsize*foldnumber+foldsize))
    train_split = train_split.astype(int)
    test_split = test_split.astype(int)
    print(test_split)

    #put the data in the destination array:
    test_data = [data[i] for i in test_split]
    test_targets = [targets[i] for i in test_split]
    train_data = [data[i] for i in train_split]
    train_targets = [targets[i] for i in train_split]

    return train_data,train_targets,test_data,test_targets;


#Definition of weight calculation function
def calculateWeight(precision,A,B):
    FPRweight = 1/((1-precision)*B+A)
    return FPRweight

#Function in order to determine, which parameter work best with the weight calculation in a specified range
def getBestParams(votes,testTargets,a_range,b_range):
    #Find the best forumla
    best_perf =[0,0.1,0.1]
    current_perf = [0,0,0]
    model_weights_normal = [0,0,0,0]
    model_weights_attack = [0,0,0,0]
    performances = []
    i = 0
    #generate empty vote array
    vote_clf=[]
    for x in range(0,len(testTargets)):
        vote_clf.append(0)
    #Generate Weights matrix
    weights=[]
    for j in range(0,len(testTargets)):
        weights.append(np.array([0,0]).astype(float))

    #Calculate classifier performances
    p = 0
    for p in range(len(votes)):
        perfs = getPerformance(testTargets,votes[p])
        performances.append(perfs)



    for a in a_range:           #Iterate through all A
        for b in b_range:       #Iterat through all B
            #Calibrate weights based on a and b
            for i in range(len(votes)): #Go through each classifier performance and calc weights
                c_perf = performances[i]
                #return true_pr,true_nr,false_pr,false_nr
                #This calculation here changes in every loop
                model_weights_normal[i] = calculateWeight(c_perf[1],a,b)
                model_weights_attack[i] = calculateWeight(c_perf[0],a,b)
                #Add the weights to matrix
                j=0 #go through each index
                for cl in votes[i]:   #go through every vote
                    if cl == 0:
                        weights[j][0] = weights[j][0] + model_weights_normal[i] #and add the weight to the matrix for the specific vote
                    else:
                        weights[j][1] = weights[j][1] + model_weights_attack[i] #and add the weight to the matrix for the specific vote
                    j=j+1

            #Carry out vote-classif

            x=0
            for x in range(0,len(testTargets)):
                #Hier kommen bisher nur 0er an
                if weights[x][0] >= weights[x][1]:
                    vote_clf[x]=0
                else:
                    vote_clf[x]=1
            current_perf = getPerformance(testTargets,vote_clf)
            if current_perf[4] > best_perf[0]:
                best_perf = [current_perf[4],a,b]
                printStats(testTargets,vote_clf,'VOTE')
            #empty variables and current weights
            i=x=j=0
            old_clf = vote_clf
            for j in range(len(weights)):
                weights[j][0] = 0
                weights[j][1] = 0
    print('best achieved:' + str(best_perf[0]) + ' With values: ' + str(a) + '(a), ' + str(b)+ '(b)')
    return best_perf

#Function in order to determine, which parameter work best with the weight.
#uses defined ranges
def findFormulaParams(votes,TestTargets):
    #First, use very large range
    a_range = np.arange(0.01,10,0.05)
    b_range = np.arange(0.01,10,0.05)
    oldRangeA = a_range[0]
    oldRangeB = b_range[0]
    old_achieved = [0,0,0]
    #make 10 times smaller granulation
    for i in range(5):
        newParams = getBestParams(votes,TestTargets,a_range,b_range)
        a_range = np.arange(newParams[1]/5,newParams[1]*2,newParams[1]/5)
        b_range = np.arange(newParams[2]/5,newParams[2]*2,newParams[2]/5)
        if oldRangeB == b_range[0]:   #The same ranges are set for A
            a_range = np.arange(a_range[4],a_range[6],b_range/5)
            b_range = np.arange(b_range[4],b_range[6],b_range/5)
        print('Testing new A-ranges from: ' + str(a_range[0]) + ' to: ' + str(a_range[len(a_range)-1]))
        print('Testing new B-ranges from: ' + str(b_range[0]) + ' to: ' + str(b_range[len(b_range)-1]))
        if old_achieved[0] != newParams[0]:
            old_achieved = newParams
    print(old_achieved)
    return old_achieved