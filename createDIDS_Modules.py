import KDDTools
import ClfTools
import numpy as np
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn import clone
from sklearn.svm import SVC

def replaceTargets(targets):
    for i in range(len(targets)):
        if targets[i] != 0:
            targets[i]=1
    return targets

#Defined Weight Function
def calculateWeight(precision):
    FNRweight = 1/((1-precision)+0.01)
    print('FNR-Weigth: ' +str(FNRweight))
    return FNRweight

#Define Models to use
HAN_models = [AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70),
              SVC(C=10.0, kernel='rbf', degree=3, gamma=0.5, coef0=0.0, tol=0.001, cache_size=500, verbose=True)]
HAN_modNames = ['DecisionTree','SVM']


NAN_models = [DecisionTreeClassifier(max_depth=None,class_weight='auto',min_samples_leaf=20),
              AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70),
              SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, cache_size=500, verbose=True)]
NAN_modNames = ['DecisionTree','AdaBoost','SVM']


WAN_models = [DecisionTreeClassifier(max_depth=None,class_weight='auto',min_samples_leaf=20),
              AdaBoostClassifier(DecisionTreeClassifier(max_depth=3),n_estimators=70),
              KNeighborsClassifier(n_neighbors=1,weights='distance',algorithm='kd_tree',leaf_size=50),
              SVC(C=1.0, kernel='rbf', degree=3, gamma=0.0, coef0=0.0, tol=0.001, cache_size=500, verbose=True)]
WAN_modNames = ['DecisionTree','AdaBoost','KNeighbour','SVM']


#Datasets with 4 Attack types
training_filename = 'data/NSL4/KDDTrain+.arff'
test_filename = 'data/NSL4/KDDTest+.arff'

#map the datasets
trainData, trainTargets = KDDTools.genFromNSL(training_filename, True, True)
testData, testTargets = KDDTools.genFromNSL(test_filename, True, True)
#scale complete set
trainData, testData = ClfTools.scaleAtan(trainData,testData)


# HAN-specific IDS Data: DDos(2),Probe(3),Normal(0),
HAN_trainData,HAN_trainTargets = KDDTools.makeDataset(trainData,trainTargets,[0,2,3])
HAN_testData,HAN_testTargets = KDDTools.makeDataset(testData,testTargets,[0,2,3])
#Convert into 2-Class problem
HAN_trainTargets = replaceTargets(HAN_trainTargets)
HAN_testTargets = replaceTargets(HAN_testTargets)
#Create calibration & test sets
HAN_calibrationData,HAN_calibrationTargets,HAN_testData,HAN_testTargets = ClfTools.randomSampling(HAN_testData, HAN_testTargets,0.5)
#Save the test Data
KDDTools.saveDataset('data/SGDIDS/HAN_testset',HAN_testData,HAN_testTargets)


# NAN-specific IDS Data: DDos(2),Probe(3),Normal(0),U2R(4)
NAN_trainData,NAN_trainTargets = KDDTools.makeDataset(trainData,trainTargets,[0,2,3])
NAN_testData,NAN_testTargets = KDDTools.makeDataset(testData,testTargets,[0,2,3])
NAN_trainTargets = replaceTargets(NAN_trainTargets)
NAN_testTargets = replaceTargets(NAN_testTargets)
NAN_calibrationData,NAN_calibrationTargets,NAN_testData,NAN_testTargets =  ClfTools.randomSampling(NAN_testData, NAN_testTargets,0.5)
KDDTools.saveDataset('data/SGDIDS/NAN_testset',NAN_testData,NAN_testTargets)


# WAN-specific IDS Data: Use full set
trainTargets = replaceTargets(trainTargets)
testTargets = replaceTargets(testTargets)
WAN_calibrationData,WAN_calibrationTargets,WAN_testData,WAN_testTargets = ClfTools.randomSampling(testData, testTargets,0.5)
KDDTools.saveDataset('data/SGDIDS/WAN_testset',NAN_testData,NAN_testTargets)



###################################################
##############WAN-Training########################
##################################################


#Set parameters for training the WAN model
i = 0
votes = []
WAN_weights_normal = []
WAN_weights_attack = []
WAN_weights = []
for j in range(0,len(testTargets)):
    WAN_weights.append(np.array([0,0]).astype(float))

#Train and test model
for model in WAN_models:
    clf = clone(model)
    clf.fit(trainData, trainTargets)
    ClfTools.createPersistent(clf,'persistent/SGDIDS/WAN/'+WAN_modNames[i])
    votes.append(clf.predict(WAN_calibrationData)) #string with predictions

    #Calibrate weights
    perfs = ClfTools.getPerformance(WAN_calibrationTargets,votes[i]) # return true_pr,true_nr,false_pr,false_nr
    WAN_weights_normal.append(calculateWeight(perfs[1]))
    WAN_weights_attack.append(calculateWeight(perfs[0]))

    #Add weights to matrix
    j=0
    for cl in votes[i]:   #go through every vote
        if cl == 0:
            WAN_weights[j][0] = WAN_weights[j][0] + WAN_weights_normal[i] #and add the weight to the matrix for the specific vote
        else:
            WAN_weights[j][1] = WAN_weights[j][1] + WAN_weights_attack[i] #and add the weight to the matrix for the specific vote
        j=j+1

    #Print the classifier performance
    ClfTools.printStats(WAN_calibrationTargets,votes[i],WAN_modNames[i])

    i=i+1

#Save WAN-Weight array
np.savetxt('persistent/SGDIDS/WAN/WAN_weights_attack.txt',WAN_weights_attack)
np.savetxt('persistent/SGDIDS/WAN/WAN_weights_normal.txt',WAN_weights_normal)


#Execute Vote-Classification & print stats
vote_clf = []
for x in range(0,len(WAN_calibrationTargets)):
    if WAN_weights[x][0] >= WAN_weights[x][1]:  #if vote is bigger or equal FOR normal, use normal...
        vote_clf.append(0)
    else:
        vote_clf.append(1)

ClfTools.printStats(WAN_calibrationTargets,vote_clf,'VOTE')

###################################################
##############HAN-Training########################
##################################################




#Set parameters for training the WAN model
i = 0
j= 0
votes = []
HAN_weights_normal = []
HAN_weights_attack = []
HAN_weights = []
for j in range(0,len(HAN_calibrationTargets)):
    HAN_weights.append(np.array([0,0]).astype(float))

#Train and test model
for model in HAN_models:
    clf = clone(model)
    clf.fit(HAN_trainData, HAN_trainTargets)
    ClfTools.createPersistent(clf,'persistent/SGDIDS/HAN/'+HAN_modNames[i])
    votes.append(clf.predict(HAN_calibrationData)) #string with predictions

    #Calibrate weights
    perfs = ClfTools.getPerformance(HAN_calibrationTargets,votes[i]) # return true_pr,true_nr,false_pr,false_nr
    HAN_weights_normal.append(calculateWeight(perfs[1]))
    HAN_weights_attack.append(calculateWeight(perfs[0]))

    #Add weights to matrix
    j=0
    for cl in votes[i]:   #go through every vote
        if cl == 0:
            HAN_weights[j][0] = HAN_weights[j][0] + HAN_weights_normal[i] #and add the weight to the matrix for the specific vote
        else:
            HAN_weights[j][1] = HAN_weights[j][1] + HAN_weights_attack[i] #and add the weight to the matrix for the specific vote
        j=j+1

    #Print the classifier performance
    ClfTools.printStats(HAN_calibrationTargets,votes[i],HAN_modNames[i])

    i=i+1

#Save WAN-Weight array
np.savetxt('persistent/SGDIDS/HAN/HAN_weights_attack.txt',HAN_weights_attack)
np.savetxt('persistent/SGDIDS/HAN/HAN_weights_normal.txt',HAN_weights_normal)


#Execute Vote-Classification & print stats
vote_clf = []
for x in range(0,len(HAN_calibrationTargets)):
    if HAN_weights[x][0] >= HAN_weights[x][1]:  #if vote is bigger or equal FOR normal, use normal...
        vote_clf.append(0)
    else:
        vote_clf.append(1)

ClfTools.printStats(HAN_calibrationTargets,vote_clf,'VOTE')



###################################################
##############NAN-Training########################
##################################################




#Set parameters for training the WAN model
i = 0
votes = []
NAN_weights_normal = []
NAN_weights_attack = []
NAN_weights = []
for j in range(0,len(NAN_calibrationData)):
    NAN_weights.append(np.array([0,0]).astype(float))

#Train and test model
for model in NAN_models:
    clf = clone(model)
    clf.fit(NAN_trainData, NAN_trainTargets)
    ClfTools.createPersistent(clf,'persistent/SGDIDS/NAN/'+NAN_modNames[i])
    votes.append(clf.predict(NAN_calibrationData)) #string with predictions

    #Calibrate weights
    perfs = ClfTools.getPerformance(NAN_calibrationTargets,votes[i]) # return true_pr,true_nr,false_pr,false_nr
    NAN_weights_normal.append(calculateWeight(perfs[1]))
    NAN_weights_attack.append(calculateWeight(perfs[0]))

    #Add weights to matrix
    j=0
    for cl in votes[i]:   #go through every vote
        if cl == 0:
            NAN_weights[j][0] = NAN_weights[j][0] + NAN_weights_normal[i] #and add the weight to the matrix for the specific vote
        else:
            NAN_weights[j][1] = NAN_weights[j][1] + NAN_weights_attack[i] #and add the weight to the matrix for the specific vote
        j=j+1

    #Print the classifier performance
    ClfTools.printStats(NAN_calibrationTargets,votes[i],NAN_modNames[i])

    i=i+1

#Save WAN-Weight array
np.savetxt('persistent/SGDIDS/NAN/NAN_weights_attack.txt',NAN_weights_attack)
np.savetxt('persistent/SGDIDS/NAN/NAN_weights_normal.txt',NAN_weights_normal)


#Execute Vote-Classification & print stats
vote_clf = []
for x in range(0,len(NAN_calibrationTargets)):
    if NAN_weights[x][0] >= NAN_weights[x][1]:  #if vote is bigger or equal FOR normal, use normal...
        vote_clf.append(0)
    else:
        vote_clf.append(1)


ClfTools.printStats(NAN_calibrationTargets,vote_clf,'VOTE')
